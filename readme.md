# Description
Further extrapolation on the education &amp; certifications section of my r&eacute;sum&eacute; dealing specifically with my learning experience at lynda.com. 

## education &amp; certifications
 - **lynda.com, Online Training** Open&#45;enrollment courses in professional Web&#45;development (12 months), March 2009 &#45; 2010

## educaci&oacute;n y certificaciones
 - **lynda.com cursos en linea** Entrenamiento y cursos libres en linea para desarrollo de web profesional (12 meses duraci&oacute;n), marzo 2009 a 2010

### Disclaimer
Every single certificate is in my name with the exception of a single .pdf that is buried in a subfolder.

#### Thank you for your interest
PEACE.